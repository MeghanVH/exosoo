﻿using Exo1.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo1
{
    class Program
    {
        static void Main(string[] args)
        {
            Personne user = new Personne();
            user.Nom = "Van Hoye";
            user.Prenom = "Meghan";
            user.DateNaiss = new DateTime(1992, 5, 9);

            Courant compte = new Courant();
            compte.Titulaire = user;
            compte.Numero = "5636542256";
            Console.WriteLine($"Compte de {compte.Titulaire.Prenom} {compte.Titulaire.Nom} dont le numéro est : {compte.Numero}.");
            compte.Depot(100);
            Console.WriteLine($"Le solde est de {compte.Solde}");
            compte.Retrait(50);
            Console.WriteLine($"Le solde est de {compte.Solde}");
            Console.ReadKey();
        }
    }
}
