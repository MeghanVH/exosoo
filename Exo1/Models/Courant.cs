﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exo1.Models
{
    class Courant
    {
        #region Variables
        private string _numero;
        private double _solde;
        private double _ligneDeCredit = 0;
        private Personne _titulaire; 
        #endregion

        public string Numero
        {
            get
            {
                return _numero;
            }
            set
            {
                _numero = value;
            }
        }
        public double Solde
        {
            get
            {
                return _solde;
            }
        }
        public double LigneDeCredit
        {
            get
            {
                return _ligneDeCredit;
            }
        }
        public Personne Titulaire
        {
            get
            {
                return _titulaire;
            }
            set
            {
                _titulaire = value;
            }
        }
        public void Retrait(double Montant)
        {
            if (LigneDeCredit < 0 && (LigneDeCredit - Montant <= 0))
            {
                Console.WriteLine("Retrait impossible");
            }
            else
            {
                this._solde = this._solde - Montant;
            }
        }
        public void Depot(double Montant)
        {
            this._solde = this._solde + Montant;

        }
    }
}
